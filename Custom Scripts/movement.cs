﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Text;
using System.Net;
using System.Net.Sockets;
using System;
using UnityEngine.SceneManagement;

public class movement : MonoBehaviour {
    Rigidbody rgb;
    LineRenderer liner;
    public Transform start;
    public float vel = 30;
    float[] casts;
    string host = "localhost";
    int port = 9090;
    Socket socket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
    
    void Start () {
        transform.position = start.position;
        rgb = GetComponent<Rigidbody>();
        liner = GetComponent<LineRenderer>();
        liner.SetPosition(0, transform.position);
        liner.startColor = Color.green;
        liner.endColor = Color.green;
        liner.positionCount = 4;
        socket.ReceiveTimeout = 1000;
        socket.SendTimeout = 1000;
        socket.Connect(host, port);
    }
	
	void Update () {
        //better use UDP
        if (socket.Connected)
        {
            Network();
        }
        else
        {
            Local();
        }
    }

    void Network()
    {
        if (socket.Connected)
        {
            byte[] commands = new byte[1024];
            socket.Receive(commands);
            string s = new string(Encoding.UTF8.GetChars(commands));
            string[] cmd = s.Split(' ');
            int Vert = Convert.ToInt32(cmd[0]);
            int Horiz = Convert.ToInt32(cmd[1]);
            Movement(Vert, Horiz);
            casts = Casting(20);
            socket.Send(Encoding.ASCII.GetBytes(casts.ToString()));
        }
    }
    void Local()
    {
        Movement(Input.GetAxis("Vertical"),Input.GetAxis("Horizontal"));
        casts = Casting(20);
    }

    float[] Casting(float maxD)
    {

        liner.SetPosition(0, transform.position);
        Vector3 start = transform.position+Vector3.up*5;
        Vector3[] dir=  new Vector3[3];
        dir[0] = transform.TransformDirection(Vector3.forward + Vector3.left);
        dir[2] = transform.TransformDirection(Vector3.forward + Vector3.right);
        dir[1] = transform.TransformDirection(Vector3.forward);
        float[] result = new float[3];
        RaycastHit hit = new RaycastHit();
        if (Physics.Raycast(new Ray(start, dir[0]), out hit, maxD))
        {
            Debug.DrawRay(start, dir[0], Color.red);
            liner.SetPosition(1, hit.transform.position);
            result[0] = hit.distance;
        }
        else
        {
            Debug.DrawRay(start, dir[0], Color.green);
            liner.SetPosition(1, transform.position);
            result[0] = -1;
        }

        if (Physics.Raycast(new Ray(start, dir[1]), out hit, maxD))
        {
            Debug.DrawRay(start, dir[1], Color.red);
            liner.SetPosition(2, hit.transform.position);
            result[1] = hit.distance;
        }
        else
        {
            Debug.DrawRay(start, dir[1], Color.green);
            liner.SetPosition(2, transform.position);
            result[1] = -1;
        }
        if(Physics.Raycast(new Ray(start, dir[2]), out hit, maxD))
        {
            Debug.DrawRay(start, dir[2], Color.red);
            liner.SetPosition(3, hit.transform.position);
            result[2] = hit.distance;
        }
        else
        {
            Debug.DrawRay(start, dir[2], Color.green);
            liner.SetPosition(3, transform.position);
            result[2] = -1;
        }
        return result;
    }

    void Movement(float vert,float horiz)
    {
        float roty = transform.rotation.eulerAngles.y * (Mathf.PI / 180);
        float frw = vert* vel;
        float side = horiz;
        if (frw > 0) transform.Rotate(Vector3.up, Mathf.Asin(side) * frw / vel);
        if (frw < 0) transform.Rotate(Vector3.up, Mathf.Asin(side) * frw / vel);
        Vector3 acl = new Vector3(frw * Mathf.Sin(roty), 0, frw * Mathf.Cos(roty));
        rgb.velocity = acl;
        GetComponent<Rigidbody>().AddRelativeForce(Vector3.right * side, ForceMode.Acceleration);
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.tag != "World")
        {
            //   socket.Close();
            //   SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
            transform.position = start.position;
        }
    }
}
