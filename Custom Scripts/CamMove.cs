﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.Experimental.UIElements;

public class CamMove : MonoBehaviour {

    int velocity;
    Camera cam;
    GameObject str_btn;
    Vector3 lnd;

    // Use this for initialization
    void Start () {
        velocity = 3;
        cam = this.GetComponent<Camera>();
        str_btn = GameObject.Find("Start_btn");
        lnd = GameObject.Find("Landscape").transform.position;
    }
	
	// Update is called once per frame
	void Update () {


        float newX = transform.localPosition.x + velocity * Input.GetAxis("Horizontal");
        float newY = transform.localPosition.y + Input.GetAxis("Mouse ScrollWheel")* 10 * velocity;
        float newZ = transform.localPosition.z + velocity * Input.GetAxis("Vertical");
        transform.localPosition = new Vector3(newX,newY,newZ);
        if (Input.GetMouseButtonDown(0))
        {
            //поймать пересечение с поверхностью, там поставить точку старта
            Vector3 pos = Input.mousePosition;
            Ray ray = cam.ScreenPointToRay(pos);
            RaycastHit hit;
            if (Physics.Raycast(ray, out hit))
            {
                pos = hit.point;
            }
            //pos.y = 0;
            if (EventSystem.current.currentSelectedGameObject != str_btn)
                GameObject.Find("Start").transform.position = pos; //+ Vector3.up * lnd.y;
        }

        if (Input.GetMouseButtonDown(1))
        {
            //поймать пересечение с поверхностью, там поставить точку старта
            Vector3 pos = Input.mousePosition;
            Ray ray = cam.ScreenPointToRay(pos);
            RaycastHit hit;
            if (Physics.Raycast(ray, out hit))
            {
                pos = hit.point;
            }
            //pos.y = 0;
            GameObject.Find("Destination").transform.position = pos;// + Vector3.up * lnd.y;
        }

    }

}
