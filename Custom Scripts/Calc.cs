﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.AI;

public class Calc : MonoBehaviour {
    GameObject exectuor;
    GameObject land;
    Transform start, dest;
    public Material mat;
    Vector3[] vertices;
    int[] trigs;

    struct Poly {
        public Vector3 center;
        public List<Poly> neib;
        public Vector3[] ind;
    }

    List<Poly> polygons;
    Poly pS, pF;



    void Awake() {
        polygons = new List<Poly>();
        start = GameObject.Find("Start").transform;
        dest = GameObject.Find("Destination").transform;
        exectuor = GameObject.Find("Executor");
        land = GameObject.Find("Landscape");
        Vector3 front = Vector3.zero, back = Vector3.zero;
        foreach (Transform child in exectuor.transform)
        {
            if (child.parent.name == "front") front = child.position;
            if (child.parent.name == "back") back = child.position;
        }


    }

    public void Bake()
    {


        List<NavMeshBuildSource> buildSources = new List<NavMeshBuildSource>();
        //LayerMask lM = -1;

        NavMeshBuilder.CollectSources(
            land.transform,
            -1,
            NavMeshCollectGeometry.PhysicsColliders,
            0,
            new List<NavMeshBuildMarkup>(),
            buildSources);

        //Debug.Log(buildSources[0]);
        NavMeshAgent a = GetMeshAgent();

        NavMeshBuildSettings buildSettings = NavMesh.GetSettingsByID(a.agentTypeID);

        buildSettings.agentHeight = a.height;
        buildSettings.agentRadius = a.radius;
        //  buildSettings.voxelSize = 1f;
        //buildSettings.minRegionArea = 1f;
        //buildSettings.tileSize = 1;

        NavMeshData navData = NavMeshBuilder.BuildNavMeshData(
           buildSettings,
           buildSources,
           new Bounds(Vector3.zero, new Vector3(10000, 10000, 10000)),
           land.transform.position + Vector3.down,
           Quaternion.Euler(Vector3.up));

        NavMesh.AddNavMeshData(navData);
        NavMeshTriangulation curr = NavMesh.CalculateTriangulation();
        Mesh navig = new Mesh();

        navig.vertices = curr.vertices;
        navig.triangles = curr.indices;
        navig.RecalculateNormals();
        //        land.GetComponent<MeshFilter>().mesh = navig;
        //      land.GetComponent<MeshCollider>().sharedMesh = navig;
        //    land.GetComponent<MeshRenderer>().material = mat;

        Debug.Log(curr.vertices.Length);
        Debug.Log(curr.indices.Length);

        //a.SetDestination(dest.position); //включить для работы NavMeshAgent
        //     Connection con = new Connection("localhost",9090);
        //    con.MakeConnection();
        //   con.Send(curr.vertices);
        //  con.Send(curr.indices);
        // con.CloseConnection();
        GetPolys(curr);
    }



    NavMeshAgent GetMeshAgent()
    {
        return exectuor.GetComponent<NavMeshAgent>();
    }

    void GetPolys(NavMeshTriangulation curr)
    {
        Poly p;

        for (int i = 0; i < curr.indices.Length; i += 3)
        {
            p.center = (curr.vertices[curr.indices[i]] + curr.vertices[curr.indices[i + 1]] + curr.vertices[curr.indices[i + 2]]) / 3;
            p.ind = new Vector3[3] { curr.vertices[curr.indices[i]], curr.vertices[curr.indices[i + 1]], curr.vertices[curr.indices[i + 2]] };
            p.neib = new List<Poly>();
            polygons.Add(p);

            foreach (Poly pl in polygons)
            {
                if (p.ind.Intersect(pl.ind).ToArray<Vector3>().Length >= 2)
                {
                    p.neib.Add(pl);
                    pl.neib.Add(p);
                }
            }

        }
        pS = GetStartPoint(start);
        pF = GetFinishPoint(dest);
        //polygons = GetPath(pS);
        polygons = NonRecGetPath();
        DrawLines();
    }

    Vector3 CalculateBezierPoint(float t, Vector3 p0, Vector3 p1, Vector3 p2, Vector3 p3)
    {
        float u = 1 - t;
        float tt = t * t;
        float uu = u * u;
        float uuu = uu * u;
        float ttt = tt * t;

        Vector3 p = uuu * p0;    //first term
        p += 3 * uu * t * p1;    //second term
        p += 3 * u * tt * p2;    //third term
        p += ttt * p3;           //fourth term

        return p;
    }

    void DrawLines()
    {
        for (int k = 0; k< polygons.Count-4;k+=3)
        {
            // foreach (Poly pl in p.neib)
            //     Debug.DrawLine(p.center, pl.center, Color.green);
            // Debug.Log(p.neib.Count);    
            Debug.DrawLine(polygons[k].center, polygons[k+1].center, Color.green);
            for (int i = 0; i <= 50; ++i)
            {
                float t = i / (float)50;
                Vector3 pixel = CalculateBezierPoint(t, polygons[k].center, polygons[k+1].center, polygons[k+2].center, polygons[k+3].center);
                Debug.DrawLine(pixel, pixel + Vector3.up, Color.blue);    //assume this function can handle Vector3
            }
        }
        Debug.DrawRay(pF.center, Vector3.up, Color.red);
    }

    private void Update()
    {
        DrawLines();
    }

    Poly GetStartPoint(Transform start)
    {
        Poly p = polygons[0];
        float minDist, dist = 0f;
        minDist = 10000f;
        for (int i = 0; i < polygons.Count; i++)
        {
            dist = Vector3.Distance(polygons[i].center, start.position);
            if (minDist > dist)
            {
                p = polygons[i];
                minDist = dist;
            }
        }
        return p;
    }

    Poly GetFinishPoint(Transform finish)
    {
        Poly p = polygons[0];
        float minDist, dist = 0f;
        minDist = 10000f;
        for (int i = 0; i < polygons.Count; i++)
        {
            dist = Vector3.Distance(polygons[i].center, finish.position);
            if (minDist > dist)
            {
                p = polygons[i];
                minDist = dist;
            }
        }
        return p;
    }

    List<Poly> path = new List<Poly>();
    Stack<Poly> stk = new Stack<Poly>();
    List<Poly> que = new List<Poly>();
    private HashSet<Poly> used = new HashSet<Poly>();


    private int Cmp(Poly x, Poly y)
    {
        float d1 = Vector3.Distance(pF.center, x.center);
        float d2 = Vector3.Distance(pF.center, y.center);
        if (d1 < d2) return -1;
        else return 1;
    }

/*
    Poly GetPath(Poly cur)
    {
        

        if (cur.center == pF.center || used.Contains(cur)) //если текущая вершина - точка финиша или уже посещена
        {
            //path.Add(cur);//поместили в список вершин пути
            return cur; //вернуть путь
        }
        else
        {
            used.Add(cur);  //пометить вершину как посещенную
            List<Poly> temp = new List<Poly>();
            foreach (Poly i in cur.neib)
            {  //поместили всех непосещенных соседей в очередь
                if (!used.Contains(i))
                    temp.Add(i);
            }
            temp.Sort(Cmp); //сортируем по удалению
            foreach (Poly i in temp)
            {
                que.Enqueue(i);
                Poly v = que.Dequeue();
                return GetPath(v);
            }
            return cur;
        }

    }
    */

    List<Poly> NonRecGetPath()
    {
        path.Clear();
        que.Add(pS);
        while (que.Count > 0) { //пока очередь не пуста 
            Poly v = que.First<Poly>();
            que.Remove(v);
            if (used.Contains(v)) continue;
            else used.Add(v);
            path.Add(v);

            if (v.center == pF.center)
            {
                Debug.Log("Found a finish ");
                Debug.Log(path.Count);
                //return path;
                break;
            }

            foreach (Poly i in v.neib)
            {
                //if(!que.Contains(i))
                    que.Add(i);
            }
            que.Sort(Cmp);
        }
        return path;
    }
}
/*
 *             for (int i = 0; i <= 50; ++i)
            { 
                    float t = i / (float)50;
                    Vector3 pixel = CalculateBezierPoint(t, p0, p1, p2, p3);
                    Debug.DrawLine(pixel, pixel+Vector3.up, Color.blue);    //assume this function can handle Vector3
            }
            Debu



    */