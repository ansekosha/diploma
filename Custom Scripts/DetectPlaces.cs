﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class DetectPlaces : MonoBehaviour {
    public GameObject start;
    public GameObject executor;
    public GameObject btn;
    // Use this for initialization
   
    public void Beg()
    {
        executor = Instantiate(executor);
        executor.transform.position = start.transform.position + Vector3.up * 2;
        executor.name = "Executor";
        //executor.SetActive(true);
        Camera.SetupCurrent(executor.GetComponentInChildren<Camera>());
        GameObject temp = GameObject.Find("Landscape");
        temp.transform.GetChild(0).gameObject.SetActive(false);
        executor.GetComponent<WheelDrive>().OnInsta();
        GameObject.Find("path").SetActive(false);
        btn.SetActive(true);
        

    }
    
}
