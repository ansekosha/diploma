﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class DebugNormals : MonoBehaviour {

	// Update is called once per frame
	void Start () {
        NavMeshTriangulation curr = NavMesh.CalculateTriangulation();
        for (int i = 0; i < curr.indices.Length-3; i+=3)
        {
            Vector3 center = (curr.vertices[i+0] + curr.vertices[i+1] + curr.vertices[i+2])/3;
            
            Debug.DrawRay(center, Vector3.up, Color.green);
        }

        for (int i = 0; i < curr.vertices.Length; i++)
        {
            Vector3 center = curr.vertices[i];

            Debug.DrawRay(center, Vector3.up, Color.red);
        }
    }
}
