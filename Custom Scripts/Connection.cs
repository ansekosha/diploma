﻿using System.Collections;
using System.Collections.Generic;
using System.Net;
using System.IO;
using System.Net.Sockets;
using UnityEngine;

public class Connection : MonoBehaviour
{

    string host;
    int port;
    Socket socket;
    public Connection(string i_host, int i_port)
    {
        host = i_host;
        port = i_port;
        socket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
    }

    public bool MakeConnection()
    {
        socket.ReceiveTimeout = 1000;
        socket.SendTimeout = 1000;
        socket.Connect(host, port);
        return socket.Connected;
    }

    public void CloseConnection()
    {
        socket.Close();
    }

    public void Send(Vector3[] msg)
    {
        NetworkStream stream = new NetworkStream(socket);
        BinaryWriter writer = new BinaryWriter(stream);
        for (int i = 0; i < msg.Length; i++) {
            writer.Write(msg[i][0]);
            writer.Write(msg[i][1]);
            writer.Write(msg[i][2]);
        }
        writer.Write('\n');
        writer.Close();
        stream.Close();
    }

    public void Send(int[] msg)
    {
        NetworkStream stream = new NetworkStream(socket);
        BinaryWriter writer = new BinaryWriter(stream);
        for (int i = 0; i < msg.Length; i++)
        {
            writer.Write(msg[i]);
            writer.Write(msg[i]);
            writer.Write(msg[i]);
        }
        writer.Write('\n');
        writer.Close();
        stream.Close();
    }
}