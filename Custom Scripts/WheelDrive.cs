﻿using UnityEngine;
using System;
using System.Net;
using System.Net.Sockets;
using System.Text;
using UnityEngine.SceneManagement;

[Serializable]
public enum DriveType
{
	RearWheelDrive,
	FrontWheelDrive,
	AllWheelDrive
}

public class WheelDrive : MonoBehaviour
{
    [Tooltip("Maximum steering angle of the wheels")]
	public float maxAngle = 30f;
	[Tooltip("Maximum torque applied to the driving wheels")]
	public float maxTorque = 300f;
	[Tooltip("Maximum brake torque applied to the driving wheels")]
	public float brakeTorque = 30000f;
	[Tooltip("If you need the visual wheels to be attached automatically, drag the wheel shape here.")]
	public GameObject wheelShape;

	[Tooltip("The vehicle's speed when the physics engine can use different amount of sub-steps (in m/s).")]
	public float criticalSpeed = 5f;
	[Tooltip("Simulation sub-steps when the speed is above critical.")]
	public int stepsBelow = 5;
	[Tooltip("Simulation sub-steps when the speed is below critical.")]
	public int stepsAbove = 1;

	[Tooltip("The vehicle's drive type: rear-wheels drive, front-wheels drive or all-wheels drive.")]
	public DriveType driveType;

    private WheelCollider[] m_Wheels;
//-------------------------------------
    Rigidbody rgb;
    LineRenderer liner;
    Transform start;
    Transform finish;
    public float vel = 30;
    float[] casts;
    string host = "localhost";
    int port = 9090;
    Socket socket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
//-------------------------------------
    // Find all the WheelColliders down in the hierarchy.
    void Start()
	{
		m_Wheels = GetComponentsInChildren<WheelCollider>();

		for (int i = 0; i < m_Wheels.Length; ++i) 
		{
			var wheel = m_Wheels [i];

			// Create wheel shapes only when needed.
			if (wheelShape != null)
			{
				var ws = Instantiate (wheelShape);
				ws.transform.parent = wheel.transform;
			}
		}

        //transform.position = start.position;
        rgb = GetComponent<Rigidbody>();
        liner = GetComponent<LineRenderer>();
        liner.SetPosition(0, transform.position);
        liner.startColor = Color.green;
        liner.endColor = Color.green;
        liner.positionCount = 4;
        socket.ReceiveTimeout = 1000;
        socket.SendTimeout = 1000;
    //    socket.Connect(host, port);

    }


    public void OnInsta()
    {
        start = GameObject.Find("Start").transform;
        finish = GameObject.Find("Destination").transform;
        transform.rotation.SetLookRotation(finish.position);
    }
    // This is a really simple approach to updating wheels.
    // We simulate a rear wheel drive car and assume that the car is perfectly symmetric at local zero.
    // This helps us to figure our which wheels are front ones and which are rear.
    void Update()
    {
        //better use UDP
       // if (socket.Connected)
       // {
       //     Network();
       // }
      //  else
      //  {
            Local();
     //   }
    }

    void Network()
    {
        if (socket.Connected)
        {
            byte[] commands = new byte[1024];
            socket.Receive(commands);
            string s = new string(Encoding.UTF8.GetChars(commands));
            string[] cmd = s.Split(' ');
            int Vert = Convert.ToInt32(cmd[0]);
            int Horiz = Convert.ToInt32(cmd[1]);
            Movement(Vert, Horiz);
            casts = Casting(500);
            string cast_result = "";

            for (int i = 0; i< casts.Length; i++)
            {
                cast_result += casts[i].ToString()+";";
            }
            //Debug.Log(cast_result);

            socket.Send(Encoding.ASCII.GetBytes(cast_result));
            
        }
    }
    void Local()
    {
        Movement(Input.GetAxis("Vertical"), Input.GetAxis("Horizontal"));
        casts = Casting(20);
    }

    float[] Casting(float maxD)
    {

        liner.SetPosition(0, transform.position);
        Vector3 start = transform.position;
        Vector3[] dir = new Vector3[3];
        dir[0] = transform.TransformDirection(Vector3.forward + Vector3.left);
        dir[2] = transform.TransformDirection(Vector3.forward + Vector3.right);
        dir[1] = transform.TransformDirection(Vector3.forward);
        float[] result = new float[3];
        RaycastHit hit = new RaycastHit();
        if (Physics.Raycast(new Ray(start, dir[0]), out hit, maxD))
        {
            Debug.DrawRay(start, dir[0], Color.red);
            liner.SetPosition(1, hit.point);
            result[0] = hit.distance;
        }
        else
        {
            Debug.DrawRay(start, dir[0], Color.green);
            liner.SetPosition(1, transform.position);
            result[0] = -1;
        }

        if (Physics.Raycast(new Ray(start, dir[1]), out hit, maxD))
        {
            Debug.DrawRay(start, dir[1], Color.red);
            liner.SetPosition(2, hit.point);
            result[1] = hit.distance;
        }
        else
        {
            Debug.DrawRay(start, dir[1], Color.green);
            liner.SetPosition(2, transform.position);
            result[1] = -1;
        }

        if (Physics.Raycast(new Ray(start, dir[2]), out hit, maxD))
        {
            Debug.DrawRay(start, dir[2], Color.red);
            liner.SetPosition(3, hit.point);
            result[2] = hit.distance;
        }
        else
        {
            Debug.DrawRay(start, dir[2], Color.green);
            liner.SetPosition(3, transform.position);
            result[2] = -1;
        }

        return result;
    }




    void Movement(float vert, float horiz)
	{
		m_Wheels[0].ConfigureVehicleSubsteps(criticalSpeed, stepsBelow, stepsAbove);

		float angle = maxAngle * horiz;
		float torque = maxTorque * vert;

        float handBrake = Input.GetKey(KeyCode.X) ? brakeTorque : 0;

		foreach (WheelCollider wheel in m_Wheels)
		{
			// A simple car where front wheels steer while rear ones drive.
			if (wheel.transform.localPosition.z > 0)
				wheel.steerAngle = angle;

			if (wheel.transform.localPosition.z < 0)
			{
				wheel.brakeTorque = handBrake;
			}

			if (wheel.transform.localPosition.z < 0 && driveType != DriveType.FrontWheelDrive)
			{
				wheel.motorTorque = torque;
			}
			if (wheel.transform.localPosition.z >= 0 && driveType != DriveType.RearWheelDrive)
			{
				wheel.motorTorque = torque;
			}

			// Update visual wheels if any.
			if (wheelShape) 
			{
				Quaternion q;
				Vector3 p;
				wheel.GetWorldPose (out p, out q);

				// Assume that the only child of the wheelcollider is the wheel shape.
				Transform shapeTransform = wheel.transform.GetChild (0);
				shapeTransform.position = p;
				shapeTransform.rotation = q;
			}
		}
	}

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.tag != "World")
        {
            //   socket.Close();
            //SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
            this.GetComponent<Rigidbody>().isKinematic = true;
            transform.position = start.position + Vector3.up * 2;
            transform.rotation = Quaternion.identity;
            this.GetComponent<Rigidbody>().isKinematic = false;
        }
    }
}
