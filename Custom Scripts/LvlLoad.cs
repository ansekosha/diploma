﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using System;



public class LvlLoad : MonoBehaviour {
    
    public GameObject landscape;
    public UnityEngine.UI.InputField input_F;
    string Dir = "D:\\Diploma\\AssetBundles\\StandaloneWindows\\";
    public Material matr;
    public Shader shade;

    public void Load()
    {


        string FPath = input_F.text;
        Debug.Log(FPath);
        if (FPath.Contains(".jpg") || FPath.Contains(".png"))
        {
            //D:\Diploma\Assets\Texture\taranaki.png
            //then begin HeightMap working for landscape generating    
            //Look, I've change some piece
            Texture2D img = new Texture2D(2,2);
            byte[] file = File.ReadAllBytes(FPath);

            bool er = img.LoadImage(file);

            List<Vector3> verts = new List<Vector3>();
            List<int> tris = new List<int>();
            int res = Mathf.Min(img.width, img.height);
            //Bottom left section of the map, other sections are similar
            for (int i = 0; i < res; i++)
            {
                for (int j = 0; j < res; j++)
                {
                    //Add each new vertex in the plane
                    verts.Add(new Vector3(i, img.GetPixel(i, j).grayscale * 100, j));
                    //Skip if a new square on the plane hasn't been formed
                    if (i == 0 || j == 0) continue;
                    //Adds the index of the three vertices in order to make up each of the two tris
                    tris.Add(res * i + j); //Top right
                    tris.Add(res * i + j - 1); //Bottom right
                    tris.Add(res * (i - 1) + j - 1); //Bottom left - First triangle
                    tris.Add(res * (i - 1) + j - 1); //Bottom left 
                    tris.Add(res * (i - 1) + j); //Top left
                    tris.Add(res * i + j); //Top right - Second triangle
                }
            }

            Vector2[] uvs = new Vector2[verts.Count];
            for (var i = 0; i < uvs.Length; i++) //Give UV coords X,Z world coords
                uvs[i] = new Vector2(verts[i].x, verts[i].z);

            
            landscape.AddComponent<MeshFilter>();
            landscape.AddComponent<MeshRenderer>();
            landscape.AddComponent<MeshCollider>();
            Mesh procMesh = new Mesh();
            procMesh.vertices = verts.ToArray(); //Assign verts, uvs, and tris to the mesh
            procMesh.uv = uvs;
            procMesh.triangles = tris.ToArray();
            procMesh.RecalculateNormals(); //Determines which way the triangles are facing


            Color[] cls = new Color[procMesh.vertices.Length];
            System.Random r = new System.Random();

            for (int i = 0; i < cls.Length; i++)
            {
                cls[i] = Color.cyan;
                cls[r.Next(0, cls.Length - 1)] = Color.red;
            }

            procMesh.colors = cls;
            matr.shader = shade;

            landscape.GetComponent<MeshFilter>().mesh = procMesh;
            landscape.GetComponent<MeshCollider>().sharedMesh = procMesh;
            landscape.GetComponent<MeshRenderer>().material = matr;
            
        }
        else
        {
            Debug.Log(Application.streamingAssetsPath);
            AssetBundle myLoadedAssetBundle = AssetBundle.LoadFromFile(Path.Combine(Dir, FPath));
            if (myLoadedAssetBundle == null)
            {
                Debug.Log("Failed to load AssetBundle!");
                return;
            }
            Mesh[] prefab = myLoadedAssetBundle.LoadAllAssets<Mesh>(); //first model in bundle
            Debug.Log(prefab[0].colors32.Length);
            Color[] cls = new Color[prefab[0].vertices.Length];
            System.Random r = new System.Random();

            for(int i = 0; i < cls.Length; i++)
            {
                cls[i] = Color.cyan;
                cls[r.Next(0, cls.Length - 1)] = Color.red;
            }
            prefab[0].colors = cls;
            matr.shader = shade;
                landscape.AddComponent<MeshFilter>();
                landscape.AddComponent<MeshRenderer>();
                landscape.AddComponent<MeshCollider>();
                landscape.GetComponent<MeshFilter>().mesh = prefab[0];
                landscape.GetComponent<MeshCollider>().sharedMesh = prefab[0];
                landscape.GetComponent<MeshRenderer>().material = matr;

        }
        Detect();
    }

    public void Detect()
    {
        GameObject land = GameObject.Find("Landscape");
        GameObject gam = new GameObject("Camera");
        gam.transform.parent = land.transform;
        gam.AddComponent<CamMove>();
        Camera cam = gam.AddComponent<Camera>();
        Debug.Log(cam);
        gam.transform.localPosition = new Vector3(0f, 0f, 0f);
        gam.transform.Rotate(new Vector3(90, 0, 0));
        Camera.SetupCurrent(cam);
    }
}
