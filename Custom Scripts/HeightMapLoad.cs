﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

    public class HeightMapLoad : MonoBehaviour
    {


        public HeightMapLoad()
        {

        }

        static public TerrainData Convert(TerrainData terrain, Texture2D heightmap)
    {
        int w = heightmap.width;
        int h = heightmap.height;
        int w2 = terrain.heightmapWidth;
        Debug.Log(w + " h:" + h + " w2:" + w2);
        float[,] heightmapData = terrain.GetHeights(0, 0, w2, w2);
        Color[] mapColors = heightmap.GetPixels();
        Color[] map = new Color[w2 * w2];

        if (w2 != w || h != w)
        {
            // Resize using nearest-neighbor scaling if texture has no filtering
            if (heightmap.filterMode == FilterMode.Point)
            {
                float dx = w / w2;
                float dy = h / w2;
                for (int y = 0; y < w2; y++)
                {
                    int thisY = (int)dy * y * w;
                    int yw = y * w2;
                    for (int x = 0; x < w2; x++)
                    {
                        map[yw + x] = mapColors[thisY + (int)dx * x];
                    }
                }
            }
            // Otherwise resize using bilinear filtering
            else
            {
                float ratioX = 1.0f / (w2 / (w - 1));
                float ratioY = 1.0f / (w2 / (h - 1));
                for (int y = 0; y < w2; y++)
                {
                    int yy = (int)Mathf.Floor(y * ratioY);
                    int y1 = yy * w;
                    int y2 = (yy + 1) * w;
                    int yw = y * w2;
                    for (int x = 0; x < w2; x++)
                    {
                        int xx = (int)Mathf.Floor(x * ratioX);

                        Color bl = mapColors[y1 + xx];
                        Color br = mapColors[y1 + xx + 1];
                        Color tl = mapColors[y2 + xx];
                        Color tr = mapColors[y2 + xx + 1];

                        var xLerp = x * ratioX - xx;
                        map[yw + x] = Color.Lerp(Color.Lerp(bl, br, xLerp), Color.Lerp(tl, tr, xLerp), y * ratioY - yy);
                    }
                }
            }
        }
        else
        {
            // Use original if no resize is needed
            map = mapColors;
        }

        // Assign texture data to heightmap
        for (int y = 0; y < w2; y++)
        {
            for (int x = 0; x < w2; x++)
            {
                heightmapData[y, x] = map[y * w2 + x].grayscale;
            }
        }
        terrain.SetHeights(0, 0, heightmapData);
        return terrain;
    }
    }
